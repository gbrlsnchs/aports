# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Contributor: Robert Sacks <robert@sacks.email>
# Maintainer: Robert Sacks <robert@sacks.email>
pkgname=streamlink
pkgver=5.0.0
pkgrel=0
pkgdesc="CLI for extracting streams from various websites to a video player of your choosing"
url="https://streamlink.github.io/"
arch="noarch"
license="BSD-2-Clause"
depends="
	python3
	py3-lxml
	py3-isodate
	py3-pycryptodome
	py3-pycountry
	py3-requests
	py3-pysocks
	py3-websocket-client
	"
makedepends="
	py3-setuptools
	py3-build
	py3-installer
	py3-wheel
	py3-versioningit
	"
checkdepends="
	py3-pytest
	py3-freezegun
	py3-requests-mock
	py3-mock
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/streamlink/streamlink/releases/download/$pkgver/$pkgname-$pkgver.tar.gz"

build() {
	# Bypass versioningit by setting the default to what we want
	sed -i -E "s|^(default-version =).*$|\1 \"$pkgver\"|" pyproject.toml

	python3 -m build --no-isolation --wheel
}

check() {
	TZ=UTC PYTHONPATH="$builddir/src" python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/$pkgname-$pkgver-py3-none-any.whl

	install -Dm644 docs/_build/man/$pkgname.1 \
		"$pkgdir"/usr/share/man/man1/$pkgname.1

	install -Dm644 completions/bash/$pkgname \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 completions/zsh/_$pkgname \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
99f6c1cc64ed33438c123ff77d536f6cee53309ec27447ffc5780217231ae06d5dbb982e9274fba8c4d39253115bc48f67f5d25e9946f0f710812c8c81be6c06  streamlink-5.0.0.tar.gz
"
