# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.32.0
pkgrel=0
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
arch="$arch !s390x"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"
options="net !check" # needs tinygo to turn go into wasm for tests

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make build VERSION=$pkgver
}

check() {
	make test
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
3a31fea34f9d8f021eb3a1d522ef47e555d7559eba50b7d64bb25aeef9fd99fc4146316f1e176dcc7ce02033b20c1f6cc7cafee563d9c54f9e4301fbf9db0b5c  trivy-0.32.0.tar.gz
"
